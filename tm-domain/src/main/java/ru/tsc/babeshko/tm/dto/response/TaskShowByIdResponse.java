package ru.tsc.babeshko.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Task;

@NoArgsConstructor
public final class TaskShowByIdResponse extends AbstractTaskResponse {

    public TaskShowByIdResponse(@Nullable final Task task) {
        super(task);
    }

}