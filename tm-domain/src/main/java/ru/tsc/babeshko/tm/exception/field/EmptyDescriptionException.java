package ru.tsc.babeshko.tm.exception.field;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}