package ru.tsc.babeshko.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.repository.IProjectRepository;
import ru.tsc.babeshko.tm.api.repository.ITaskRepository;
import ru.tsc.babeshko.tm.api.service.IConnectionService;
import ru.tsc.babeshko.tm.api.service.IProjectTaskService;
import ru.tsc.babeshko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.babeshko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.repository.ProjectRepository;
import ru.tsc.babeshko.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    public ITaskRepository getTaskRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    public IProjectRepository getProjectRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @NotNull String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(projectId);
            taskRepository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @NotNull String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(null);
            taskRepository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            try {
                taskRepository.removeTasksByProjectId(projectId);
                projectRepository.removeById(projectId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

}