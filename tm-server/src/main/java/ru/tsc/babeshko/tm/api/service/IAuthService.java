package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Session;
import ru.tsc.babeshko.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}