package ru.tsc.babeshko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.babeshko.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.babeshko.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.babeshko.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.babeshko.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.babeshko.tm.api.service.IPropertyService;
import ru.tsc.babeshko.tm.dto.request.*;
import ru.tsc.babeshko.tm.dto.response.ProjectCreateResponse;
import ru.tsc.babeshko.tm.dto.response.TaskCreateResponse;
import ru.tsc.babeshko.tm.dto.response.TaskShowByIdResponse;
import ru.tsc.babeshko.tm.dto.response.UserLoginResponse;
import ru.tsc.babeshko.tm.marker.ISoapCategory;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.service.PropertyService;

@Category(ISoapCategory.class)
public final class ProjectTaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Nullable
    private Project initProject;

    @Nullable
    private Task initTask;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("user", "user"));
        token = loginResponse.getToken();
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse createProjectResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "test", "test", null, null)
        );
        initProject = createProjectResponse.getProject();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse createTaskResponse = taskEndpoint.createTask(
                new TaskCreateRequest(token, "test", "test", null, null)
        );
        initTask = createTaskResponse.getTask();
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(null, initTask.getId(), initProject.getId())));
        projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, initTask.getId(), initProject.getId()));
        @NotNull final TaskShowByIdResponse response = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, initTask.getId()));
        Assert.assertNotNull(response.getTask().getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, initTask.getId(), initProject.getId()));
        @NotNull final TaskShowByIdResponse response = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, initTask.getId()));
        Assert.assertNotNull(response.getTask().getProjectId());
        projectTaskEndpoint.unbindTaskFromProject(
                new TaskUnbindFromProjectRequest(token, initTask.getId(), initProject.getId()));
        @NotNull final TaskShowByIdResponse responseAfterUnbind = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, initTask.getId()));
        Assert.assertNull(responseAfterUnbind.getTask().getProjectId());
    }

}